# RELEASE NOTES FOR KOHA 18.12.00
22 Feb 2019

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 18.12.00 can be downloaded from:

- [Download](http://download.koha-community.org/koha-18.05-latest.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 18.12.00 is a major release, that comes with many new features.

It includes 5 new features, 99 enhancements, 179 bugfixes.



## New features

### REST api

- [[16497]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16497) Add API routes for libraries
- [[17006]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17006) Add route to change patron's password

> Municipal Libray Ceska Trebova


- [[22061]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22061) Public route to change password
- [[22132]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22132) Add Basic authentication to the REST API

> This adds http BASIC authentication as an optional auth method to the RESTful APIs. This greatly aids developers when developing against our APIs.



### Web services

- [[17047]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17047) Mana Knowledge Base : share data

## Enhancements

### Acquisitions

- [[16939]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16939) Making all 'add to basket' actions buttons

> Sponsored by Catalyst IT

- [[19850]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19850) Enhance invoicing functionality for each line item
- [[21427]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21427) Format prices on ordered/spent lists
- [[21966]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21966) Fix descriptions of acquisition permissions to be more clear (again)

### Architecture, internals, and plumbing

- [[18789]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18789) Send a Koha::Patron object to the templates

> RMaint Note: Followup in bug 21928 causes this bug to appear in release note.. remove before release!


- [[19458]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19458) Self-check module highlighting
- [[21002]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21002) Add Koha::Account::add_debit

> Sponsored by PTFS Europe

- [[21727]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21727) Add Koha::Account::Line->adjust

> Sponsored by PTFS Europe

- [[21896]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21896) Add Koha::Account::reconcile_balance

> Adds a business logic level routine for reconciling user account balances.


- [[21912]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21912) Koha::Objects->search lacks tests
- [[21980]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21980) Add some new Exceptions for Koha::Account methods

> Sponsored by PTFS Europe

- [[21992]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21992) Remove Koha::Patron::update_password
- [[21993]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21993) Be userfriendly when the CSRF token is wrong
- [[21999]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21999) C4::Circulation::AddIssue uses DBIx::Class directly
- [[22003]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22003) Remove unused subroutines displaylog and GetLogStatus from in C4::Log
- [[22026]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22026) Remove `use  Modern::Perl` from Koha::REST::classes
- [[22047]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22047) set_password should have a 'skip_validation' param
- [[22048]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22048) Use set_password instead of update_password in the codebase
- [[22049]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22049) MarkIssueReturned should rely on returndate only
- [[22051]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22051) Make Koha::Object->store translate 'Incorrect <type> value' exceptions
- [[22144]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22144) Add method metadata() to Koha::Biblio
- [[22194]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22194) Add Koha::Exceptions::Metadata

### Authentication

- [[21547]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21547) Use set_password in opac-passwd and remove sub goodkey

> Architectural enhancement backported to 18.11.x series to aid future backports. There should be no noticeable effects for the end user.



### Cataloging

- [[21826]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21826) Automatic authority record generation improvements

> Sponsored by National Library of Finland


### Circulation

- [[11373]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11373) Add "change calculation" feature to the fine payment forms
- [[17353]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17353) Add phone number column to checkout search
- [[18816]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18816) Make CataloguingLog work in production by preventing circulation from spamming the log
- [[19066]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19066) Add branchcode to accountlines
- [[21754]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21754) If an item is marked as lost, any outstanding transfers upon it should be automatically cancelled

> Sponsored by Brimbank Library, Australia


> If an item is marked as lost, then any pending transfers the item had will now be removed.


- [[21844]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21844) Add callnumber to fines descriptions

### Command-line Utilities

- [[18562]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18562) Add koha-sip script to handle SIP servers for instances

> To ease multi-tenant sites maintenance, several handy scripts were introduced. For handling SIP servers, 3 scripts were introduced: koha-start-sip, koha-stop-sip and koha-enable-sip.  
This patch introduces a new script, koha-sip, that unifies those actions regarding SIP servers on a per instance base, through the use of option switches.  
18.11 Note: The introduction of this script does NOT remove the koha-*-sip versions at this time and the scripts can be used interchangeably until one upgrades to 19.05 where the koha-*-sip versions are officially removed.


- [[22238]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22238) Remove koha-*-sip scripts in favor of koha-sip

> The new koha-sip maintenance script replaces the old koha-start-sip, koha-stop-sip and koha-enable-sip scripts. This patch removes them, while keeping backwards compatibility (i.e. you can still run them until you get used to the new syntax).



### Course reserves

- [[21446]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21446) Improve display of changed values on course reserves and show permanent location instead of cart

### Database

- [[21753]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21753) issuingrules.chargename is unused and should be removed
- [[22155]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22155) biblio_metadata.marcflavour should be renamed 'schema'

### Fines and fees

- [[21578]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21578) 'Pay fines' tab incorrectly describes the purpose
- [[21918]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21918) Clean up pay fines template
- [[22148]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22148) Cancelling some payments/writeoffs redirects to unexpected page

### Hold requests

- [[19770]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19770) Add cardnumber to holds awaiting pickup screen and add classes to borrower info

### I18N/L10N

- [[21789]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21789) Example usage of I18N Template::Toolkit plugin

### ILL

- [[20581]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20581) Allow manual selection of custom ILL request statuses
- [[20600]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20600) Provide the ability for users to filter ILL requests
- [[20639]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20639) Allow setting a default/single backend for OPAC driven requests
- [[20640]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20640) Allow migrating a request between backends

### Installation and upgrade (web-based installer)

- [[20000]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20000) use Modern::Perl in installer perl scripts

> Sponsored by Catalyst IT


> Code cleanup which improves the readability, and therefore reliability, of Koha.



### Lists

- [[12759]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=12759) Add ability to pass list contents to batch record modification/deletion tools

> Sponsored by Catalyst IT


### MARC Bibliographic record staging/import

- [[19164]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19164) Allow MARC modification templates to be used in staged MARC imports

### Notices

- [[21241]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21241) Set suggestion notices message_transport_type to sms if syspref is enabled and patron has an smsalertnumber but no email address

> If the FallbackToSMSIfNoEmail syspref is enabled then when a borrower has no email address but does have a smsalertnumber then suggestion notice message_transport_type is set to sms.



### OPAC

- [[14272]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14272) Allow OPAC to show a single news entry

> Sponsored by Catalyst IT


> Allows to display OPAC news entries on their own page. The news entry will remain accessible by direct URL even after the entry itself has expired.


- [[21399]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21399) Sort patron fines in OPAC by date descending as a default
- [[21850]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21850) Remove search request from page title of OPAC result list
- [[21871]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21871) Show authority 856 links in the OPAC
- [[22029]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22029) Remove Google+ from social links on OPAC detail

> Google revealed that Google Plus accounts will be shut down on April 2, 2019.


- [[22102]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22102) Markup fixes for OPAC article request page

### Patrons

- [[16276]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16276) When automatically deleting expired borrowers, make sure they didn't log in recently
- [[17854]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17854) New Print slip and close button next to Close button
- [[22198]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22198) Add granular permission setting for Mana KB

### Reports

- [[22147]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22147) Hide 'Batch modify' button when printing reports

### Staff Client

- [[12283]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=12283) Set autocomplete=off for patron search input

### System Administration

- [[18143]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18143) Silence floody MARC framework export
- [[22190]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22190) Add column configuration to patron category administration
- [[22191]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22191) Add column configuration to libraries administration

### Templates

- [[13618]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=13618) Add additional template filter methods and a filter presence test to Koha

> RMaint Note: The inclusion of bug 22007 in 18.11.02 means this enhancement bug gets added to the release notes.. It should be removed before release!


- [[20102]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20102) Remove attribute "text/css" for <style> element used in staff client templates
- [[20569]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20569) Improve description of CheckPrevCheckout system preference

> A simple string patch that clarifies the intention of the CheckPrevCheckout system preference options.


- [[20729]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20729) Update style of datepickers
- [[20809]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20809) Link patron image to patron image add/edit form
- [[21436]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21436) Switch two-column templates to Bootstrap grid: Tools part 4
- [[21438]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21438) Switch two-column templates to Bootstrap grid: Patron card creator
- [[21442]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21442) Switch two-column templates to Bootstrap grid: Circulation part 1
- [[21449]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21449) Switch two-column templates to Bootstrap grid: Circulation part 2
- [[21569]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21569) Switch two-column templates to Bootstrap grid: Circulation part 3
- [[21573]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21573) Move lists barcode and biblionumber entry form to modal
- [[21672]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21672) Switch templates to Bootstrap grid: Various
- [[21693]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21693) Clean up checkout notes template
- [[21695]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21695) Clean up access files template
- [[21785]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21785) Add column configuration to hold ratios report
- [[21790]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21790) Switch error page template to Bootstrap grid
- [[21792]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21792) Switch two-column templates to Bootstrap grid: Serials part 3
- [[21795]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21795) Switch two-column templates to Bootstrap grid: Notices and slips
- [[21797]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21797) Update two-column templates with Bootstrap grid: Acquisitions part 5
- [[21803]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21803) Redesign authorized values interface
- [[21913]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21913) Clean up payment details page
- [[21945]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21945) Clean up stock rotation template
- [[21963]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21963) Switch two-column templates to Bootstrap grid: Patrons part 1
- [[21964]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21964) Switch two-column templates to Bootstrap grid: Patrons part 2
- [[21965]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21965) Switch two-column templates to Bootstrap grid: Patrons part 3
- [[22015]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22015) Move DataTables CSS to global include
- [[22104]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22104) Clean up patron API keys template
- [[22134]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22134) Add account expiration information to patron details
- [[22195]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22195) Change default DataTables configuration to consolidate buttons
- [[22196]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22196) Clean up Mana KB administration template
- [[22261]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22261) Revise style of DataTables menus

### Test Suite

- [[21798]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21798) We need t::lib::TestBuilder::build_sample_biblio
- [[21817]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21817) Mock userenv should be a t::lib::Mocks method

> Test suite enhancement backported to 18.11.x series to aid future backports. There should be no noticeable effects for the end user.


- [[21971]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21971) TestBuilder::build_sample_item

### Tools

- [[18661]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18661) Make "Replace only included patron attributes" default on patron import

> On the 'import patrons' page, the "Replace all patron attributes" is automatically selected which is the more dangerous option. This patch sets the default selection as "Replace only included patron attributes" as it is a safer option.



### Web services

- [[19945]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19945) ILSDI - Return the reason a reserve is impossible

### importedbugs

- [[18939]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18939) Record matching rules didnot work by matching with callnumber

> See bug 18939 - typo in commit messages




## Critical bugs fixed

(This list includes all bugfixes since the previous major version. Most of them
have already been fixed in maintainance releases)

### Acquisitions

- [[18723]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18723) Dot not recognized as decimal separator on receive
- [[21605]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21605) Cannot create EDI account
- [[21989]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21989) JS error in "Add orders from MARC file" - addorderiso2709.pl
- [[22282]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22282) Internal software error when exporting basket group as PDF
- [[22293]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22293) Sticky toolbar making vendor form uneditable

### Architecture, internals, and plumbing

- [[21610]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21610) Koha::Object->store needs to handle incorrect values
- [[21910]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21910) Koha::Library::Groups->get_search_groups should return the groups, not the children
- [[21955]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21955) Cache::Memory should not be used as L2 cache

> Cache::Memory fails to work correctly under a plack environment as the cache cannot be shared between processes.


- [[22052]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22052) DeleteExpiredOpacRegistrations should skip bad borrowers
- [[22388]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22388) svc/split_callnumbers should have execute flag set

### Authentication

- [[21973]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21973) CAS URL escaped twice, preventing login

### Cataloging

- [[21986]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21986) Quotation marks are wrongly escaped in several places
- [[22140]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22140) More use of EasyAnalyticalRecords pref

### Circulation

- [[18805]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18805) Currently it is impossible to apply credits against debits in patron accounts

> This patch adds an `Apply Credits` button to the accounts interface to allow a librarian to apply outstanding credits against outstanding debits.


- [[21065]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21065) Data in account_offsets and accountlines is deleted with the patron leaving gaps in financial reports
- [[21491]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21491) When 'Default lost item fee refund on return policy' is unset it says no but acts as if 'yes'
- [[21915]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21915) Add a way to automatically reconcile balance for patrons

> Sponsored by ByWater Solutions


> In the past, if a patron had any credit existing on their account (newly added, or pre-existing), if debts were present then the credit balance would always be immediately applied to the debt.  This functionality was inadvertently removed during refactoring efforts which debuted in 16.11.  
This patch adds code to restore the functionality and allows it to be optionally applied to the system via a new system preference, `AccountAutoReconcile`.  
Note: The new preference defaults to the post 16.11 behaviour, if you wish to restore the 16.11 functionality then you will need to update the preference after the upgrade.


- [[21928]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21928) CircAutoPrintQuickSlip 'clear' is not working
- [[22020]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22020) Configure Columns for Patron Issues checkin hides renewal

### Database

- [[13515]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=13515) Table messages is missing FK constraints and is never cleaned up
- [[21931]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21931) Upgrade from 3.22 fails when running updatedatabase.pl script

### Fines and fees

- [[22301]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22301) Paying fines is broken when using CurrencyFormat = FR

### Hold requests

- [[21495]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21495) Regression in hold override functionality
- [[21608]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21608) Arranging holds priority with dropdowns is faulty when there are waiting/intransit holds

### I18N/L10N

- [[21895]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21895) Translations fail on upgrade to 18.11.00 (package installation)

### Installation and upgrade (web-based installer)

- [[22024]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22024) Update translated web installer files with new class splitting rules

### MARC Authority data support

- [[21962]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21962) The `searching entire record` option in authority searches is currently failing

### OPAC

- [[21911]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21911) Scoping OPACs by branch does not work with new library groups
- [[21950]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21950) Searching with 'accents' breaks on navigating to the second page of results
- [[22030]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22030) OverDrive requires configuration for field passed as username
- [[22085]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22085) UNIMARC default XSLT broken by Bug 14716

### Patrons

- [[21778]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21778) Sorting is inconsistent on patron search based on permissions
- [[22253]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22253) Koha throws an exception when updating a borrower with an insecure password
- [[22386]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22386) Importing using attributes as matchpoint broken

### REST api

- [[22071]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22071) authenticate_api_request does not stash koha.user in the OAuth use case

### Reports

- [[21984]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21984) Unable to load second page of results for reports with reused parameters
- [[21991]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21991) Displaying more rows on report results does not work for reports with parameters

### Searching - Elasticsearch

- [[20261]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20261) No result in some page in authority search opac and pro (ES)

### Staff Client

- [[21405]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21405) Pagination in authorities search broken for Zebra and broken for 10000+ results in ES

### System Administration

- [[22389]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22389) Classification splitting sources regex - cannot consistentlyadd/delete

### Templates

- [[21813]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21813) In-page JavaScript causes error on patron entry page

### Test Suite

- [[21956]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21956) Sysprefs not reset by regressions.t

### Web services

- [[21738]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21738) [ILS-DI] Error placing a hold on a title without item


## Other bugs fixed

(This list includes all bugfixes since the previous major version. Most of them
have already been fixed in maintainance releases)

### About

- [[7143]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=7143) Bug for tracking changes to the about page
- [[21441]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21441) System information gives reference to a non-existant table

### Acquisitions

- [[18166]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18166) Show internal and vendor notes for received orders
- [[20865]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20865) Remove space before : on order receive filters
- [[21089]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21089) Overlapping elements in ordering information on acqui/supplier.pl
- [[21929]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21929) Typo in orderreceive.tt
- [[22110]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22110) Editing adjustments doesn't work for Currencyformat != US
- [[22171]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22171) Format shipping cost on invoice.pl with with 2 decimals

### Architecture, internals, and plumbing

- [[19816]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19816) output_pref must implement 'dateonly' for dateformat => rfc3339
- [[19920]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19920) changepassword is exported from C4::Members but has been removed
- [[21170]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21170) Warnings in MARCdetail.pl - isn't numeric in numeric eq (==)
- [[21478]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21478) Koha::Hold->suspend_hold allows suspending in transit holds
- [[21622]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21622) Incorrect GROUP BY clause in acqui/ scripts
- [[21759]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21759) Avoid manually setting amountoutstanding in _FixAccountForLostAndReturned

> This patch results in a proper offset always being recorded for auditing purposes when a user is refunded after returning a previously lost item.


- [[21788]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21788) C4::Circulation::ProcessOfflinePayment should pass library_id to ->pay
- [[21848]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21848) Resolve unac_string warning from Circulation.t
- [[21905]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21905) Plugin hook intranet_catalog_biblio_enhancements_toolbar_button incorrectly filtered
- [[21907]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21907) Error from mainpage when Article requests enabled and either IndependentBranches or IndependentBranchesPatronModifications is enabled
- [[21909]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21909) Koha::Account::outstanding_* methods should preserve call context
- [[21969]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21969) Koha::Account->outstanding_* should look for debits/credits by checking 'amount'
- [[22006]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22006) Koha::Account::Line->item should return undef if no item linked
- [[22007]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22007) KohaDates output does not need to be html filtered
- [[22033]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22033) related_resultset is a hole in the Koha::Object logic
- [[22059]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22059) Wrong exception parameters in Koha::Patron->set_password
- [[22097]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22097) CataloguingLog should be suppressed for item branch transfers
- [[22124]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22124) Update cataloguing plugin system to not generate type parameter in script tag
- [[22125]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22125) branches.pickup_location should be flagged as boolean
- [[22391]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22391) Incorrect GROUP BY in /acqui/ajax-getauthvaluedropbox.pl

### Cataloging

- [[20491]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20491) Use "Date due" in table header of item table
- [[22122]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22122) Make sequence of Z39.50 search options match in acq and cataloguing
- [[22242]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22242) Javascript error in value builder cased by Select2

### Circulation

- [[17236]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17236) Add minute and hours to last checked out item display for hourly loans
- [[17347]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17347) 'Renew' tab should ignore whitespace at begining and end of barcode
- [[21877]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21877) Show authorized value description for withdrawn in checkout
- [[22054]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22054) Display a nicer error message when trying to renew an on-site checkout from renew page
- [[22083]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22083) Typo in circulation_batch_checkouts.tt
- [[22111]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22111) Correctly format fines when placing holds (maxoutstanding warning)
- [[22119]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22119) Add price formatting in circulation
- [[22120]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22120) Add price formatting to patron summary print
- [[22130]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22130) Batch checkout : authorized value description is never shown with notforloan status.
- [[22203]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22203) Holds modal no longer links to patron
- [[22351]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22351) SCSS conversion broke style on last checked out information

### Command-line Utilities

- [[12488]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=12488) Make bulkmarcimport.pl -d use DELETE instead of TRUNCATE
- [[21908]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21908) biblio_metadata is missing from the rebuild_zebra.pl tables list
- [[22235]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22235) Make maintenance scripts use koha-sip instead of koha-*-sip
- [[22299]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22299) Typo in parameter of import_patrons.pl: preserve_extended_atributes

> The --preserve-extended-atributes parameter for import_patrons.pl had a typo within it.  In this version we have fixed the typo and so the attribute name has been updated to --preserve-extended-attributes


- [[22323]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22323) Cronjob runreport.pl has a CSV encoding issue

### Developer documentation

- [[20544]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20544) Wrong comment in database documentation for items.itemnotes
- [[21290]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21290) POD of ModItem mentions MARC for items

### Fines and fees

- [[21849]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21849) Offsets not stored correctly in _FixOverduesOnReturn
- [[22066]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22066) branchcode should be recorded for manual credits
- [[22138]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22138) members/paycollect.pl has not been updated to have the new tab names

### Hold requests

- [[7614]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=7614) Use branch transfer limits for determining available opac holds pickup locations

### Holidays

- [[21885]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21885) Improve date selection on calendar for selecting the end date on a range

### I18N/L10N

- [[21736]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21736) Localization widget messages are not translatable

### ILL

- [[22101]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22101) ILL requests missing in menu on advanced search page
- [[22121]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22121) Display 'Price paid' on ILL requests according to CurrencyFormat pref

### Installation and upgrade (command-line installer)

- [[20174]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20174) Remove xml_sax.pl target from Makefile.pl

### Installation and upgrade (web-based installer)

- [[11922]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11922) Add SHOW_BCODE patron attribute for Norwegian web installer
- [[21710]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21710) Fix typo atributes in some installer files
- [[22095]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22095) Dead link in web installer

### Lists

- [[21751]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21751) fixFloat toolbar not displaying properly in Chrome

### MARC Authority data support

- [[19994]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19994) use Modern::Perl in Authorities perl scripts
- [[21880]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21880) "Relationship information" disappears when accessing paginated results in authority searches

### MARC Bibliographic data support

- [[22034]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22034) Viewing record with Default framework doesn't work on MARC tab

### Notices

- [[21571]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21571) Translate notices fail on ACCTDETAILS
- [[21829]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21829) Date displays as a datetime in notices
- [[22002]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22002) Each message_transport_type in the letters table is showing as a separate notice in Tools > Notices and slips

### OPAC

- [[403]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=403) Reserve process allows duplicate reserves
- [[10676]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=10676) OpacHiddenItems not working for restricted on OPAC detail
- [[21192]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21192) Borrower Fields on OPAC's Personal Details Screen Use Self Register Field Options Incorrectly
- [[21335]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21335) Remove redundant includes of right-to-left.css
- [[21808]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21808) Field 711 is not handled correctly in showAuthor XSLT for relator term or code
- [[21947]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21947) Filtering order generates html in notes
- [[22058]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22058) OPAC holdings table shows &nbsp; instead of blank
- [[22118]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22118) Format hold fee when placing holds in OPAC
- [[22207]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22207) Course reserves page does not have unique body id

### Packaging

- [[21897]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21897) Typo in postinst affecting zebra configuration file installation

### Patrons

- [[19818]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19818) Add id into tag html from moremember.tt
- [[20165]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20165) Capitalization: Street Address should be Street address in patron search options
- [[21930]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21930) Typo in the manage_circ_rules_from_any_libraries description
- [[22067]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22067) Koha::Patron->can_see_patron_infos should return if no patron is passed
- [[22149]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22149) Grammar fix in the manage_circ_rules_from_any_libraries description

### REST api

- [[21786]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21786) Routes for credits should include library_id
- [[22216]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22216) Make GET /patrons/{patron_id} staff only
- [[22227]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22227) Make GET /cities staff only

### Reports

- [[20274]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20274) itemtypes.plugin report: not handling item-level_itypes syspref
- [[20679]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20679) Remove 'rows per page' from reports print layout
- [[22082]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22082) Ambiguous column in patron stats
- [[22168]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22168) Improve styling of new chart settings for reports
- [[22278]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22278) Newly created report group is not selected after saving an SQL report
- [[22287]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22287) Correct new charts CSS

### SIP2

- [[19832]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19832) SIP checkout removes extra hold on same biblio
- [[21997]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21997) SIP patron information requests can lock patron out of account

### Searching

- [[14716]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14716) Correctly URI-encode URLs in XSLT result lists and detail pages
- [[18909]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18909) Enable the maximum zebra records size to be specified per instance

### Searching - Elasticsearch

- [[21084]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21084) Searching for authorities with 'contains' gives no results if search terms include punctuation

### Searching - Zebra

- [[22073]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22073) Diacritics Ž and ž not being mapped for searching (Non-ICU)

### Serials

- [[16231]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=16231) Correct permission handling in subscription edit menu
- [[21845]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21845) Sort of issues in OPAC subscription table
- [[22156]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22156) Subscription result list sorts on "checkbox" by default
- [[22239]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22239) JavaScript error on subscription detail page when there are no orders

### Staff Client

- [[21802]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21802) Edit notices form is not aligned with accordeon headers
- [[21904]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21904) Patron search library dropdown should be limited  by group if "Hide patron info" is enabled for group

### System Administration

- [[3820]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=3820) More detailed patron record changes log
- [[7403]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=7403) Remove warning from CataloguingLog system preference
- [[15110]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=15110) Improve decreaseHighHolds system preference description
- [[21637]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21637) Capitalization: EasyAnalyticalRecords syspref option "Don't Display" should be "Don't display"
- [[21855]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21855) Remove mention of deprecated delete_unverified_opac_registrations.pl cronjob
- [[21926]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21926) Enhance OAI-PMH:archiveID system preference description
- [[21961]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21961) Typo in permission keeps Did you mean? config from showing up
- [[22009]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22009) Fix error messages for classification sources and filing rules
- [[22170]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22170) Library group description input field should be longer

### Templates

- [[8387]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=8387) Hide headings in tools when user has no permissions for any listed below
- [[10562]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=10562) Improve Leader06 Type Labels in MARC21slim2OPACResults.xsl
- [[10659]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=10659) Upgrade jQuery star ratings plugin
- [[21840]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21840) Fix some typos in the templates
- [[21866]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21866) Rephrase "Warning: This *report* was written for an older version of Koha" to refer to plugins
- [[21990]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21990) No background color for div.error, must be .alert
- [[22080]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22080) Easier translation of ElasticSearch mappings page
- [[22113]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22113) Add price formatting on item lost report
- [[22116]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22116) Add price formatting to rental charge and replacement price on items tab in staff
- [[22197]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22197) Add Mana KB link to administration sidebar menu
- [[22236]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22236) Translation should generate tags with consistent attribute order

### Test Suite

- [[14334]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14334) DBI fighting DBIx over Autocommit in tests
- [[21692]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21692) Koha::Account->new has no tests
- [[22107]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22107) Avoid deleting data in some tests
- [[22254]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22254) t/db_dependent/Koha/Patrons.t contains a DateTime math error

### Tools

- [[19915]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=19915) Inventory tool doesn't use cn_sort for callnumber ranges

> This patch brings the inventory tool inline with other pages displaying data sorted by callnumbers by also adopting the use of cn_sort for sorting.


- [[20634]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20634) Inventory form has 2 identical labels "Library:"
- [[21465]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21465) Cannot overlay patrons when matching by cardnumber if userid exists in file and in Koha
- [[21861]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=21861) The MARC modification template actions editor does not always validate user input
- [[22011]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22011) Typo in Item Batch Modification
- [[22022]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22022) Authorised values on the batch item modification page are not displayed in order (order by code, not lib)
- [[22036]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22036) Tidy up tags/review script
- [[22136]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22136) Import patrons notes hides a note because the syspref isn't referenced correctly

## New sysprefs

- AccountAutoReconcile
- AutoShareWithMana
- FallbackToSMSIfNoEmail
- Mana
- OverDriveUsername
- RESTBasicAuth
- RESTPublicAPI

## System requirements

Important notes:
    
- Perl 5.10 is required
- Zebra is required

## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/18.05/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (99.7%)
- Armenian (99.8%)
- Basque (72.5%)
- Chinese (China) (77%)
- Chinese (Taiwan) (98.7%)
- Czech (92.4%)
- Danish (63.6%)
- English (New Zealand) (95.5%)
- English (USA)
- Finnish (92.4%)
- French (98.7%)
- French (Canada) (94%)
- German (100%)
- German (Switzerland) (98.3%)
- Greek (80.5%)
- Hindi (99.8%)
- Italian (97.3%)
- Norwegian Bokmål (67.5%)
- Occitan (post 1500) (70.2%)
- Persian (52.9%)
- Polish (93.6%)
- Portuguese (99.8%)
- Portuguese (Brazil) (87.4%)
- Slovak (95.7%)
- Spanish (98.6%)
- Swedish (93.8%)
- Turkish (99.8%)
- Vietnamese (65.1%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 18.12.00 is

- Release Manager: [Nick Clemens](mailto:nick@bywatersolutions.com)
- Release Manager assistants:
  - [Jonathan Druart](mailto:jonathan.druart@bugs.koha-community.org)
  - [Tomás Cohen Arazi](mailto:tomascohen@gmail.com)
- QA Manager: [Katrin Fischer](mailto:Katrin.Fischer@bsz-bw.de)
- QA Team:
  - [Alex Arnaud](mailto:alex.arnaud@biblibre.com)
  - [Julian Maurice](mailto:julian.maurice@biblibre.com)
  - [Kyle Hall](mailto:kyle@bywatersolutions.com)
  - Josef Moravec
  - [Martin Renvoize](mailto:martin.renvoize@ptfs-europe.com)
  - [Jonathan Druart](mailto:jonathan.druart@bugs.koha-community.org)
  - [Marcel de Rooy](mailto:m.de.rooy@rijksmuseum.nl)
  - [Tomás Cohen Arazi](mailto:tomascohen@gmail.com)
  - [Chris Cormack](mailto:chrisc@catalyst.net.nz)
- Topic Experts:
  - SIP2 -- Colin Campbell
  - EDI -- Colin Campbell
  - Elasticsearch -- Ere Maijala
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
- Bug Wranglers:
  - Luis Moises Rojas
  - Jon Knight
  - [Indranil Das Gupta](mailto:indradg@l2c2.co.in)
- Packaging Manager: [Mirko Tietgen](mailto:mirko@abunchofthings.net)
- Documentation Manager: Caroline Cyr La Rose
- Documentation Team:
  - David Nind
  - Lucy Vaux-Harvey

- Translation Managers: 
  - [Indranil Das Gupta](mailto:indradg@l2c2.co.in)
  - [Bernardo Gonzalez Kriegel](mailto:bgkriegel@gmail.com)

- Wiki curators: 
  - Caroline Cyr La Rose
- Release Maintainers:
  - 18.11 -- [Martin Renvoize](mailto:martin.renvoize@ptfs-europe.com)
  - 18.05 -- Lucas Gass
  - 18.05 -- Jesse Maseto
  - 17.11 -- [Fridolin Somers](mailto:fridolin.somers@biblibre.com)
- Release Maintainer assistants:
  - 18.05 -- [Kyle Hall](mailto:kyle@bywatersolutions.com)

## Credits
We thank the following libraries who are known to have sponsored
new features in Koha 18.12.00:

- Brimbank Library, Australia
- ByWater Solutions
- Catalyst IT
- National Library of Finland
- PTFS Europe

We thank the following individuals who contributed patches to Koha 18.12.00.

- morgane alonso (2)
- Jasmine Amohia (13)
- Ethan Amohia (3)
- Aleisha Amohia (9)
- Tomás Cohen Arazi (99)
- Alex Arnaud (10)
- Christopher Brannon (3)
- Alex Buckley (4)
- Colin Campbell (1)
- Frédérick Capovilla (1)
- Nick Clemens (72)
- Jonathan Druart (60)
- Magnus Enger (1)
- Charles Farmer (1)
- Katrin Fischer (40)
- Lucas Gass (1)
- Kyle Hall (19)
- Helene Hickey (9)
- Andrew Isherwood (34)
- Te Rauhina Jackson (1)
- Mackey Johnstone (1)
- Pasi Kallinen (2)
- Jack Kelliher (2)
- Olli-Antti Kivilahti (2)
- Jon Knight (1)
- Jiří Kozlovský (1)
- Owen Leonard (65)
- Olivia Lu (4)
- Ere Maijala (5)
- Julian Maurice (4)
- Jose-Mario Monteiro-Santos (1)
- Josef Moravec (9)
- Agustin Moyano (2)
- Liz Rea (1)
- Martin Renvoize (34)
- root (2)
- Marcel de Rooy (14)
- Caroline Cyr La Rose (1)
- Andreas Roussos (3)
- Fridolin Somers (13)
- Lari Taskula (4)
- Pierre-Marc Thibault (1)
- Mirko Tietgen (1)
- Mark Tompsett (5)
- Baptiste Wojtkowski (1)
- Nazlı Çetin (1)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 18.12.00

- abunchofthings.net (1)
- ACPL (65)
- BibLibre (30)
- BSZ BW (40)
- ByWater-Solutions (92)
- Catalyst (5)
- Coeur D'Alene Public Library (3)
- Devinim (1)
- f1ebe1bec408 (2)
- Independant Individuals (35)
- jkozlovsky.cz (1)
- jns.fi (6)
- Koha Community Developers (60)
- Libeo (1)
- Libriotech (1)
- Loughborough University (1)
- PTFS-Europe (69)
- Rijks Museum (14)
- Solutions inLibro inc (4)
- stacmail.net (2)
- The City of Joensuu (2)
- Theke Solutions (101)
- University of Helsinki (5)
- Wellington East Girls' College (13)
- wgc.school.nz (9)

We also especially thank the following individuals who tested patches
for Koha.

- Hugo Agud (2)
- Ethan Amohia (1)
- Aleisha Amohia (2)
- Jasmine Amohia (2)
- Tomás Cohen Arazi (65)
- Alex Arnaud (9)
- Bob Bennhoff (7)
- Anne-Claire Bernaudin (2)
- Christopher Brannon (3)
- Mikaël Olangcay Brisebois (12)
- Barton Chittenden (1)
- Nick Clemens (565)
- David Cook (3)
- Devlyn Courtier (2)
- Frédéric Demians (1)
- Michal Denar (11)
- Devinim (5)
- Jonathan Druart (38)
- Nicole Engard (1)
- Magnus Enger (1)
- Charles Farmer (20)
- Katrin Fischer (213)
- Brendan Gallagher (4)
- Lucas Gass (7)
- Claire Gravely (13)
- Victor Grousset (1)
- Kyle Hall (97)
- Helene Hickey (2)
- Andrew Isherwood (2)
- Dilan Johnpullé (1)
- Mackey Johnstone (3)
- Jose-Mario (1)
- Pasi Kallinen (2)
- Jack Kelliher (3)
- Rhonda Kuiper (1)
- Owen Leonard (61)
- Olivia Lu (1)
- Andreas Hedström Mace (1)
- Ere Maijala (1)
- Julian Maurice (7)
- Jose-Mario Monteiro-Santos (13)
- Josef Moravec (102)
- David Nind (24)
- Eric Phetteplace (1)
- Liz Rea (2)
- Martin Renvoize (139)
- Benjamin Rokseth (2)
- Marcel de Rooy (56)
- Lisette Scheer (2)
- Maryse Simard (14)
- Pierre-Marc Thibault (31)
- Mirko Tietgen (2)
- Mark Tompsett (5)
- Te Rahui Tunua (1)
- Marc Véron (2)
- Nazlı Çetin (4)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is master.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 22 Feb 2019 15:01:48.
